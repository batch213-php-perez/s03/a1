<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity s03 for Classes and objects</title>
</head>
<body>
	
	<h1>Person</h1>

	<p><?php echo $person->printName(); ?></p>

	<h1>Developer</h1>

	<p><?php echo $developer->printName(); ?></p>

	<h1>Engineer</h1>

	<p><?php echo $engineer->printName(); ?></p>

</body>
</html>